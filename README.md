# Plato

Plato is designed for efficient visualization of particle data. Think
of it sort of like matplotlib, but being less focused on 2D plotting.

## Documentation

See the full documentation at https://plato-draw.readthedocs.io
. Documentation can also be built using sphinx:

```
cd doc
make html
```