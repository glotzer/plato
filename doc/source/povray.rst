Povray Backend
==============

.. automodule:: plato.draw.povray

.. autoclass:: Scene
   :members:

3D Graphics Primitives
----------------------

.. autoclass:: ConvexPolyhedra
   :members:

.. autoclass:: ConvexSpheropolyhedra
   :members:

.. autoclass:: Lines
   :members:

.. autoclass:: Mesh
   :members:

.. autoclass:: Spheres
   :members:
