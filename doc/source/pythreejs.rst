Pythreejs Backend
=================

.. automodule:: plato.draw.pythreejs

.. autoclass:: Scene
   :members:

3D Graphics Primitives
----------------------

.. autoclass:: ConvexPolyhedra
   :members:

.. autoclass:: Lines
   :members:

.. autoclass:: Mesh
   :members:

.. autoclass:: Spheres
   :members:
