Vispy Backend
=============

.. automodule:: plato.draw.vispy

.. autoclass:: Scene
   :members:

2D Graphics Primitives
----------------------

.. autoclass:: Arrows2D
   :members:

.. autoclass:: Disks
   :members:

.. autoclass:: Polygons
   :members:

.. autoclass:: Spheropolygons
   :members:

.. autoclass:: Voronoi
   :members:

3D Graphics Primitives
----------------------

.. autoclass:: ConvexPolyhedra
   :members:

.. autoclass:: ConvexSpheropolyhedra
   :members:

.. autoclass:: Lines
   :members:

.. autoclass:: Mesh
   :members:

.. autoclass:: SpherePoints
   :members:

.. autoclass:: Spheres
   :members:
